<?php
/**
 * Created by PhpStorm.
 * User: sergey.tsykolovskyi
 * Date: 05/11/15
 * Time: 13:26
 */


namespace rokittd\banking\models;

use yii\base\Model;

class LiqParams extends Model{

    const DESCRIPTION = 'description';
    const VERSION = 'version';
    const PUBLIC_KEY = 'public_key';
    const PRIVATE_KEY = '';
    const AMOUNT = 'amount';
    const CURRENCY = 'currency';
    const ORDER_ID = 'order_id';
    const TYPE = 'type';
    const SUBSCRIBE = 'subscribe';
    const SUBSCRIBE_DATE_START = 'subscribe_date_start';
    const SUBSCRIBE_PERIODICITY = 'subscribe_periodicity';
    const PRODUCT_URL = 'product_url';
    const SERVER_URL = 'server_url';
    const RESULT_URL = 'result_url';
    const PAY_WAY = 'pay_way';
    const LANGUAGE = 'language';
    const SANDBOX = 'sandbox';
    const VERSION_3 = 3;
    const ACTION = 'action';
    const PHONE = 'phone';
}